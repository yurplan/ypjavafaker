package com.yurplan.javafaker.utils;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.yaml.snakeyaml.Yaml;

@SuppressWarnings("unchecked")
public class YpFaker {

	private Map<String, Object> faker;
	private Random random;


	public YpFaker() {
		random = new Random();
		InputStream input = this.getClass().getClassLoader().getResourceAsStream("com/yurplan/javafaker/ressources/res.yml");
		Yaml yaml = new Yaml();
		Map<String, Object> object = (Map<String, Object>) yaml.load(input);
		Map<String, Object> en = (Map<String, Object>) object.get("en");
		faker = (Map<String, Object>) en.get("faker");

	}

	public String genFirstName(){
		Map<String, Object> name = (Map<String, Object>) faker.get("name");
		List<String> fn = (List<String>) name.get("first_name");
		String FName = fn.get(random.nextInt(fn.size()));
		return FName;
	}

	public String genLastName(){
		Map<String, Object> name = (Map<String, Object>) faker.get("name");
		List<String> fn = (List<String>) name.get("last_name");
		/*for (Map.Entry<String, Object> entry : name.entrySet()) {
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
		}*/	
		return fn.get(random.nextInt(fn.size()));
	}
	public String genEmailSuffix(){
		Map<String, Object> internet = (Map<String,Object>) faker.get("internet");
		List<String> fn = (List<String>) internet.get("free_email");
		return fn.get(random.nextInt(fn.size()));
	}
	public String genCountry(){
		Map<String, Object> country = (Map<String, Object>) faker.get("address");
		List<String> fn = (List<String>) country.get("country");		
		return fn.get(random.nextInt(fn.size()));
	}
	public String genAddress() {
		Map<String, Object> name = (Map<String, Object>) faker.get("name");
		List<String> fn = (List<String>) (name.get("last_name"));
		Map<String, Object> suffix = (Map<String, Object>) faker.get("address");
		List<String> fn2 = (List<String>) (suffix.get("city_suffix"));
		Map<String, Object> streetType = (Map<String, Object>) faker.get("address");
		List<String> fn3 = (List<String>) (streetType.get("street_suffix"));
		return (random.nextInt(100)+" "+
				fn.get(random.nextInt(fn.size()))+
				fn2.get(random.nextInt(fn2.size()))+" "+
				fn3.get(random.nextInt(fn3.size())));
	}
	public String genLorem(int wordsNb){
		String lo = null; 
		Map<String, Object> lorem = (Map<String, Object>) faker.get("lorem");
		List<String> loList = (List<String>) lorem.get("words");
		lo = loList.get(random.nextInt(loList.size()));
		for(int i = 0; i<= wordsNb; i++){
			lo = lo+" "+loList.get(random.nextInt(loList.size()));
		}
		return lo;
	}
}

package com.yurplan.javafaker.test;

import com.yurplan.javafaker.utils.YpFaker;

public class Main {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		YpFaker ypFaker = new YpFaker();
		for (int i = 0; i < 60; i++) {
			System.out.println("\n"+ypFaker.genFirstName()+" "+ypFaker.genLastName()+"\n"+ypFaker.genAddress()+"\n"+ypFaker.genCountry()+"\n");
			System.out.println("Lorem Ipsum: "+ypFaker.genLorem(100));
		}
	}

}
